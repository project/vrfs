<?php

namespace Drupal\vrfs\Plugin\Field\FieldFormatter;

use Drupal\views\ViewExecutable;
use Drupal\views\Views;
use Drupal\viewsreference\Plugin\Field\FieldFormatter\ViewsReferenceLazyFieldFormatter;

/**
 * Field formatter for Viewsreference Field.
 *
 * @FieldFormatter(
 *   id = "viewsreference_lazy_formatter_improved",
 *   label = @Translation("Views reference (lazy builder) - Improved"),
 *   field_types = {"viewsreference"}
 * )
 */
class ViewsReferenceLazyFieldFormatterImproved extends ViewsReferenceLazyFieldFormatter {

  /**
   * {@inheritdoc}
   */
  public static function lazyBuilder(string $view_name, string $display_id, string $data, string $enabled_settings, bool $plugin_types, ?string $parent_entity_type, ?string $parent_entity_id, ?string $parent_field_name): array {
    $unserialized_data = !empty($data) ? unserialize($data, ['allowed_classes' => FALSE]) : [];
    $unserialized_enabled_settings = !empty($enabled_settings) ? unserialize($enabled_settings, ['allowed_classes' => FALSE]) : [];
    $view = Views::getView($view_name);

    // Add an extra check because the view could have been deleted.
    if (!$view instanceof ViewExecutable) {
      return [];
    }

    $view->setDisplay($display_id);
    // Add properties to the view so our hook_views_pre_build() implementation
    // can alter the view. This is pretty hacky, but we need this to fix ajax
    // behaviour in views. The hook_views_pre_build() needs to know if the
    // view was part of a viewsreference field or not.
    $view->element['#viewsreference'] = [
      'data' => $unserialized_data,
      'enabled_settings' => $unserialized_enabled_settings,
      'parent_entity_type' => $parent_entity_type,
      'parent_entity_id' => $parent_entity_id,
      'parent_field_name' => $parent_field_name,
    ];

    if (!empty($parent_entity_type) && !empty($parent_entity_id)) {
      $view->storage->setThirdPartySetting('vrfs', 'entity', \Drupal::entityTypeManager()
        ->getStorage($parent_entity_type)
        ->load($parent_entity_id));
    }
    $view->preExecute();
    $view->execute($display_id);

    $render_array = $view->buildRenderable($display_id, $view->args, FALSE);
    if ($plugin_types) {
      if (!empty($view->result) || !empty($view->empty)) {
        // Add a custom template if the title is available.
        $title = $view->getTitle();
        if (!empty($title)) {
          // If the title contains tokens, we need to render the view to
          // populate the rowTokens.
          if (mb_strpos($title, '{{') !== FALSE) {
            $view->render();
            $title = $view->getTitle();
          }
          $render_array['title'] = [
            '#theme' => 'viewsreference__view_title',
            '#title' => $title,
          ];
        }
        // The views_add_contextual_links() function needs the following
        // information in the render array in order to attach the contextual
        // links to the view.
        $render_array['#view_id'] = $view->storage->id();
        $render_array['#view_display_show_admin_links'] = $view->getShowAdminLinks();
        $render_array['#view_display_plugin_id'] = $view->getDisplay()
          ->getPluginId();
        views_add_contextual_links($render_array, $render_array['#view_display_plugin_id'], $display_id);
      }
    }

    // #lazy_builder can't return elements with #type, so we need to add a
    // wrapper.
    return [$render_array];
  }

}
