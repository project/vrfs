<?php

namespace Drupal\vrfs\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\views\ViewExecutable;
use Drupal\views\Views;
use Drupal\viewsreference\Plugin\Field\FieldFormatter\ViewsReferenceFieldFormatter;

/**
 * Field formatter for Viewsreference Field.
 *
 * @FieldFormatter(
 *   id = "viewsreference_formatter_improved",
 *   label = @Translation("Views reference Improved"),
 *   field_types = {"viewsreference"}
 * )
 */
class ViewsReferenceFieldFormatterImproved extends ViewsReferenceFieldFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    // Get the information for the parent entity and field to allow
    // customizing the view / view results.
    $parent_entity_type = $items->getEntity()->getEntityTypeId();
    $parent_entity_id = $items->getEntity()->id();
    $parent_field_name = $items->getFieldDefinition()->getName();

    foreach ($items as $delta => $item) {
      $view_name = $item->getValue()['target_id'];
      $display_id = $item->getValue()['display_id'];
      $view = Views::getView($view_name);

      // Add an extra check because the view could have been deleted.
      if (!$view instanceof ViewExecutable) {
        continue;
      }

      $view->setDisplay($display_id);
      $enabled_settings = array_filter($this->getFieldSetting('enabled_settings') ?? []);

      // Add properties to the view so our hook_views_pre_build() implementation
      // can alter the view. This is pretty hacky, but we need this to fix ajax
      // behaviour in views. The hook_views_pre_build() needs to know if the
      // view was part of a viewsreference field or not.
      $view->element['#viewsreference'] = [
        'data' => unserialize($item->getValue()['data'], ['allowed_classes' => FALSE]),
        'enabled_settings' => $enabled_settings,
        'parent_entity_type' => $parent_entity_type,
        'parent_entity_id' => $parent_entity_id,
        'parent_field_name' => $parent_field_name,
      ];

      $view->storage->setThirdPartySetting('vrfs', 'entity', $item->getEntity());
      $view->preExecute();
      $view->execute($display_id);

      $render_array = $view->buildRenderable($display_id, $view->args, FALSE);
      if (!empty(array_filter($this->getSetting('plugin_types')))) {
        if (!empty($view->result) || !empty($view->empty)) {
          // Add a custom template if the title is available.
          $title = $view->getTitle();
          if (!empty($title)) {
            // If the title contains tokens, we need to render the view to
            // populate the rowTokens.
            if (mb_strpos($title, '{{') !== FALSE) {
              $view->render();
              $title = $view->getTitle();
            }
            $render_array['title'] = [
              '#theme' => $view->buildThemeFunctions('viewsreference__view_title'),
              '#title' => $title,
              '#view' => $view,
            ];
          }
          // The views_add_contextual_links() function needs the following
          // information in the render array in order to attach the contextual
          // links to the view.
          $render_array['#view_id'] = $view->storage->id();
          $render_array['#view_display_show_admin_links'] = $view->getShowAdminLinks();
          $render_array['#view_display_plugin_id'] = $view->getDisplay()->getPluginId();
          views_add_contextual_links($render_array, $render_array['#view_display_plugin_id'], $display_id);

          $elements[$delta]['contents'] = $render_array;
        }
      }
    }

    return $elements;
  }

}
