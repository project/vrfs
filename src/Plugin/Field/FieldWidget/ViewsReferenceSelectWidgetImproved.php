<?php

namespace Drupal\vrfs\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\viewsreference\Plugin\Field\FieldWidget\ViewsReferenceSelectWidget;

/**
 * Plugin implementation of the 'options_select' widget.
 *
 * @FieldWidget(
 *   id = "viewsreference_select_improved",
 *   label = @Translation("Views reference select list (Improved)"),
 *   description = @Translation("An autocomplete views select list field."),
 *   field_types = {
 *     "viewsreference"
 *   }
 * )
 */
class ViewsReferenceSelectWidgetImproved extends ViewsReferenceSelectWidget {

  /**
   * {@inheritdoc}
   */
  public function fieldElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $field_name = $this->fieldDefinition->getName();

    // Determine the element parents.
    $field_parents = [];
    if (isset($element['#field_parents'])) {
      $field_parents = $element['#field_parents'];
    }
    elseif (isset($element['target_id']['#field_parents'])) {
      $field_parents = $element['target_id']['#field_parents'];
    }

    $field_path = array_merge($field_parents, [$field_name, $delta]);
    $target_id_field_path = array_merge($field_path, ['target_id']);

    // Get the current values.
    $field_value = $this->itemCurrentValues($items, $delta, $element, $form, $form_state);

    // Setup JavaScript states.
    switch ($element['target_id']['#type']) {
      case 'select':
        $view_selected_js_state = ['!value' => '_none'];
        $ajax_event = 'change';
        $element['target_id']['#default_value'] = $field_value['target_id'] ?? '';
        break;

      default:
        $view_selected_js_state = ['filled' => TRUE];
        $ajax_event = 'viewsreference-select';
        break;
    }

    // Build our target_id field name attribute from the parent elements.
    $target_id_names = $target_id_field_path;
    $target_id_name_string = array_shift($target_id_names);
    foreach ($target_id_names as $target_id_name) {
      $target_id_name_string .= '[' . $target_id_name . ']';
    }

    // We build a unique class name from field elements and any parent elements
    // that might exist which will be used to render the display id options in
    // our ajax function.
    $html_wrapper_id = Html::getUniqueId(implode('-', $field_path));

    $class = get_class($this);

    $element['target_id']['#target_type'] = 'view';
    $element['target_id']['#limit_validation_errors'] = [];
    $element['target_id']['#ajax'] = [
      'callback' => [$class, 'itemAjaxRefresh'],
      'event' => $ajax_event,
      'wrapper' => $html_wrapper_id,
      'progress' => [
        'type' => 'throbber',
        'message' => $this->t('Getting display IDs...'),
      ],
    ];

    $display_id = $field_value['display_id'] ?? NULL;
    $view_name = $field_value['target_id'] ?? NULL;
    $options = [];
    if ($view_name) {
      // Extract the view id from the text.
      preg_match('#\((.*?)\)#', $view_name, $match);
      if (!empty($match)) {
        $view_name = $match[1];
      }
      $options = $this->getViewDisplays($view_name);
    }

    $element['display_id'] = [
      '#title' => $this->t('Display'),
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => $display_id,
      '#empty_option' => $this->t('- Select -'),
      '#empty_value' => '',
      '#weight' => 10,
      '#attributes' => [
        'class' => [
          'viewsreference-display-id',
        ],
      ],
      '#states' => [
        'visible' => [
          ':input[name="' . $target_id_name_string . '"]' => $view_selected_js_state,
        ],
        'required' => [
          ':input[name="' . $target_id_name_string . '"]' => $view_selected_js_state,
        ],
      ],
      '#ajax' => [
        'callback' => [$class, 'itemAjaxRefresh'],
        'event' => $ajax_event,
        'wrapper' => $html_wrapper_id,
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Getting options...'),
        ],
      ],
    ];

    $field_data = [];
    if (!empty($field_value['data'])) {
      $field_data = unserialize($field_value['data'], ['allowed_classes' => FALSE]);
    }

    $element['options'] = [
      '#type' => 'details',
      '#title' => $this->t('Options'),
      '#weight' => 10,
      '#states' => [
        'visible' => [
          ':input[name="' . $target_id_name_string . '"]' => $view_selected_js_state,
        ],
      ],
    ];

    $viewsreference_plugin_manager = \Drupal::service('plugin.manager.viewsreference.setting');
    $plugin_definitions = $viewsreference_plugin_manager->getDefinitions();
    $enabled_settings = array_filter($this->getFieldSetting('enabled_settings') ?? []);
    foreach ($enabled_settings as $enabled_setting) {
      if (!empty($plugin_definitions[$enabled_setting])) {
        $plugin_definition = $plugin_definitions[$enabled_setting];
        /** @var \Drupal\viewsreference\Plugin\ViewsReferenceSettingInterface $plugin_instance */
        $plugin_instance = $viewsreference_plugin_manager->createInstance($plugin_definition['id'], [
          'view_name' => $view_name,
          'display_id' => $display_id,
          'target_entity_type_id' => $this->fieldDefinition->get('entity_type'),
          'target_bundle' => $this->fieldDefinition->get('bundle'),
        ]);

        $element['options'][$plugin_definition['id']] = [
          '#title' => $plugin_definition['label'],
          '#type' => 'textfield',
          '#default_value' => $field_data[$plugin_definition['id']] ?? $plugin_definition['default_value'],
          '#states' => [
            'visible' => [
              ':input[name="' . $target_id_name_string . '"]' => $view_selected_js_state,
            ],
          ],
        ];

        $plugin_instance->alterFormField($element['options'][$plugin_definition['id']]);
      }
    }

    if (empty($enabled_settings)) {
      unset($element['options']);
    }

    $element['#attached']['library'][] = 'viewsreference/viewsreference';
    $element['#after_build'][] = [$class, 'itemResetValues'];

    // Wrap element for AJAX replacement.
    $element = [
      '#prefix' => '<div id="' . $html_wrapper_id . '">',
      '#suffix' => '</div>',
        // Pass the id along to other methods.
      '#wrapper_id' => $html_wrapper_id,
    ] + $element;

    return $element;
  }

}
