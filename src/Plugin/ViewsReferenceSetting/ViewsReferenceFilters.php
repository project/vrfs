<?php

namespace Drupal\vrfs\Plugin\ViewsReferenceSetting;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\search_api\Entity\Index;
use Drupal\views\ViewExecutable;
use Drupal\viewsreference\Plugin\ViewsReferenceSettingInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The views reference setting filters plugin.
 *
 * @ViewsReferenceSetting(
 *   id = "filters",
 *   label = @Translation("Filters"),
 *   default_value = "",
 * )
 */
class ViewsReferenceFilters extends PluginBase implements ViewsReferenceSettingInterface, ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * Key value service.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueFactoryInterface
   */
  protected $keyValue;

  /**
   * {@inheritdoc}
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $key_value
   *   Key value storage.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, KeyValueFactoryInterface $key_value) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->keyValue = $key_value;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('keyvalue')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function alterFormField(array &$form_field) {
    $form_field['#type'] = 'details';
    $form_field['#open'] = TRUE;
    $form_field['#tree'] = TRUE;

    $target_type = NULL;

    if ($this->configuration['display_id'] != NULL) {
      $view_id = $this->configuration['view_name'];
      $display_id = $this->configuration['display_id'];

      $entity_type_manager = \Drupal::service('entity_type.manager');

      /** @var \Drupal\views\ViewExecutable $view */
      $view = $entity_type_manager->getStorage('view')->load($view_id)->getExecutable();
      $view->setDisplay($display_id);
      $contextual_filters = $view->getHandlers('argument');

      if (!empty($contextual_filters)) {
        foreach ($contextual_filters as $filter) {
          $referred_bundle = NULL;
          $base_table = $view->storage->get('base_table');

          if (isset($filter['entity_type']) && !empty($field_name)) {
            $entity_type = $filter['entity_type'];
            $field_name = $filter['entity_field'];

            if (!str_contains($base_table, $entity_type)) {
              $referred_bundle = $this->getBundleReferenced($entity_type, $field_name);
            }
            $form_field[$field_name] = $this->getFormSchema($entity_type, $field_name, $referred_bundle, $form_field);
          }
          else {
            if (str_contains($base_table, 'search_api_index')) {
              $index_id = str_replace('search_api_index_', '', $base_table);
              $index = Index::load($index_id);
              $datasource = key($index->getDatasources());

              $entity_type = str_replace('entity:', '', $datasource);
              $field_name = $filter['field'];
            }
            else {
              $filter_table = $filter['table'];
              $processed_table = explode('__', $filter_table);
              $entity_type = $processed_table[0];
              $field_name = $processed_table[1];
            }

            if (!empty($entity_type) && !empty($field_name)) {
              /** @var \Drupal\Core\Entity\EntityFieldManagerInterface $field_manager */
              $field_manager = \Drupal::service('entity_field.manager');

              $entity_fields = $field_manager->getFieldStorageDefinitions($entity_type);
              $entity_field = $entity_fields[$field_name];
              $field_entity_type = $entity_field->getType();

              if ($field_entity_type == 'entity_reference') {
                if ($entity_field->getSetting('target_type') != NULL) {
                  $target_type = $entity_field->getSetting('target_type');
                }
                else {
                  $item_definition = $entity_field->getItemDefinition();
                  $target_type = $item_definition->getSetting('target_type');
                }
                $referred_bundle = $this->getBundleReferenced($entity_type, $field_name);
              }
              $form_field[$field_name] = $this->getFormSchema($target_type, $field_name, $referred_bundle, $form_field);
            }
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function alterView(ViewExecutable $view, $value) {
    if (empty($value)) {
      return;
    }

    $view_arguments = $view->getHandlers('argument');
    $arguments = [];

    foreach ($view_arguments as $view_argument) {
      $filter_table = $view_argument['table'];
      $processed_table = explode('__', $filter_table);
      $field_view_values = [];

      if (isset($processed_table[0]) && $processed_table[0] == 'taxonomy_term' && !empty($view_argument["relationship"])) {
        $field_name = $view_argument["relationship"];
      }
      elseif (isset($view_argument['entity_field'])) {
        $field_name = $view_argument['entity_field'];
      }
      elseif (isset($view_argument['field'])) {
        $field_name = str_replace('_target_id', '', $view_argument['field']);
      }
      else {
        $field_name = $processed_table[1];
      }

      if (!array_key_exists($field_name, $value)) {
        continue;
      }

      $field_values_ids = $value[$field_name]['container']['value'];

      if (is_array($field_values_ids)) {
        foreach ($field_values_ids as $field_id) {
          $field_id = array_values($field_id);
          $field_view_values[] = $field_id[0];
        }
      }
      elseif ($field_values_ids != NULL) {
        $field_view_values = explode("\r\n", $field_values_ids);
      }

      $field_values = [
        'condition' => $value[$field_name]['container']['condition'],
        'ids' => $field_view_values,
        'checkbox' => $value[$field_name]['checkbox'],
      ];

      if (($field_values['checkbox'] == 1) && ($field_values['ids'] != NULL)) {
        array_push($arguments, implode($field_values['condition'], $field_values['ids']));
      }
      else {
        $field_exception = $view_argument['exception']['value'] ?? NULL;
        $arguments[] = $field_exception;
      }
    }

    $view->setArguments($arguments);
  }

  /**
   * Autocomplete Schema Method.
   *
   * @var $referenced_entity string
   * @var $field_name string
   * @var $referred_bundle array
   * @var $form_field array
   *
   * @return array
   *   An array with the structure needed for the autocomplete element
   */
  private function getFormSchema($referenced_entity, $field_name, $referred_bundle, $form_field) {
    $default_value = $form_field['#default_value'];
    if ($referred_bundle != NULL) {
      $entity_type_manager = \Drupal::entityTypeManager();
      $entity_type = $entity_type_manager->getStorage($referenced_entity);

      $entity_label = $entity_type->getEntityType()->getKey('label');

      $selection_settings = [
        'target_bundles' => $referred_bundle['target'],
        'sort' => ['field' => $entity_label, 'direction' => 'asc'],
        'auto_create' => (BOOL) 0,
        // Even though we've specified '0' for 'auto_create', it seems that
        // a value for 'auto_crteate_bundle' is required for this to work.
        'auto_create_bundle' => '',
        'match_operator' => 'CONTAINS',
      ];

      $selection_handler = 'default:' . $referenced_entity;
      $data = serialize($selection_settings) . $referenced_entity .
        $selection_handler;
      $selection_settings_key = Crypt::hmacBase64(
        $data,
        Settings::getHashSalt()
      );

      $key_value_storage = $this->keyValue->get('entity_autocomplete');
      if (!$key_value_storage->has($selection_settings_key)) {
        $key_value_storage->set($selection_settings_key, $selection_settings);
      }

      $route_parameters = [
        'target_type' => $referenced_entity,
        'selection_handler' => $selection_handler,
        'selection_settings_key' => $selection_settings_key,
      ];
      $url = Url::fromRoute(
        'autocomplete_deluxe.autocomplete',
        $route_parameters,
        ['absolute' => TRUE]
      )->getInternalPath();

      if (isset($form_field['#default_value'][$field_name]['container']['value'])) {
        $entities = [];
        $field_values = $form_field['#default_value'][$field_name]['container']['value'];
        foreach ($field_values as $field_value) {
          $target_id = $field_value['target_id'];
          $entity = $entity_type_manager->getStorage($referenced_entity)->load($target_id);
          $entities[] = $entity;
        }
        $default_value[$field_name]['container']['value'] = self::implodeEntities($entities);
      }

      $autocomplete_schema = [
        '#title' => $referred_bundle['label'] ?? $field_name,
        '#description' => '',
        '#type' => 'autocomplete_deluxe',
        '#field_parents' => [],
        '#delta' => 0,
        '#autocomplete_deluxe_path' => $url,
        '#selection_settings' => $selection_settings,
        '#multiple' => TRUE,
        '#target_type' => $referenced_entity,
        '#selection_handler' => $selection_handler,
        '#limit' => 10,
        '#size' => 60,
        '#new_terms' => 0,
        '#min_length' => 0,
        '#delimiter' => ',',
        '#not_found_message_allow' => 0,
        '#not_found_message' => "The term '@term' will be added.",
        '#no_empty_message' => '',
        '#default_value' => $default_value[$field_name]['container']['value'] ?? '',
        '#cardinality' => FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED,
      ];
    }
    else {
      $autocomplete_schema = [
        '#type' => 'textarea',
        '#title' => $referred_bundle['label'] ?? $field_name,
        '#default_value' => $default_value[$field_name]['container']['value'] ?? '',
      ];
    }

    $checkbox_schema = [
      '#type' => 'checkbox',
      '#title' => $referred_bundle['label'] ?? $field_name,
      '#attributes' => [
        'id' => 'field_' . $field_name,
      ],
      '#default_value' => $default_value[$field_name]['checkbox'] ?? '',
    ];

    $condition_schema = [
      '#type' => 'radios',
      '#title' => $this->t('Choose condition'),
      '#default_value' => $default_value[$field_name]['container']['condition'] ?? '+',
      '#options' => [
        '+' => $this->t('Or'),
        ',' => $this->t('And'),
      ],
    ];

    $details_schema = [
      '#type' => 'details',
      '#title' => $referred_bundle['label'] ?? $field_name,
      '#tree' => TRUE,
      '#weight' => 1,
      '#states' => [
        'visible' => [
          ':input[id="field_' . $field_name . '"]' => ['checked' => TRUE],
        ],
      ],
      'value' => $autocomplete_schema,
      'condition' => $condition_schema,
    ];

    $form_schema = [
      'checkbox' => $checkbox_schema,
      'container' => $details_schema,
    ];

    return $form_schema;
  }

  /**
   * Bundle Referenced Method.
   *
   * @var $entity string
   * @var $field string
   *
   * @return array|null
   *   An array with the bundles referenced by a field in an entity
   */
  private function getBundleReferenced($entity, $field) {
    $bundle_info_service = \Drupal::service('entity_type.bundle.info');
    $bundles = $bundle_info_service->getAllBundleInfo()[$entity];
    foreach ($bundles as $id => $value) {
      $node_fields = \Drupal::service('entity_field.manager')->getFieldDefinitions($entity, $id);
      $node_field = $node_fields[$field] ?? [];
      if (!empty($node_field)) {
        if ($node_field->getType() == 'entity_reference' && !($node_field instanceof BaseFieldDefinition)) {
          $settings = $node_field->getSettings();
          switch (array_key_first($settings['handler_settings'])) {
            case 'target_bundles':
              $target_bundles['target'] = $settings['handler_settings']['target_bundles'];
              $target_bundles['label'] = $node_field->getLabel();
              break;

            case 'view':
              $view = \Drupal::entityTypeManager()
                ->getStorage('view')
                ->load($settings["handler_settings"]["view"]["view_name"])
                ->getExecutable();
              $view->initDisplay();
              $view->setDisplay($settings["handler_settings"]["view"]["display_name"]);
              $filters = $view->display_handler->getOption('filters');
              $target_bundles = $filters['vid']['value'];
              break;
          }
          return $target_bundles;
        }
      }

    }
    return NULL;
  }

  /**
   * Implodes the tags from the taxonomy module.
   *
   * @param \Drupal\Core\Entity\EntityInterface[] $entities
   *   List of entities.
   * @param string $bundle
   *   Bundle name.
   *
   * @return string
   *   Imploded list of entity labels.
   */
  public static function implodeEntities(array $entities, $bundle = NULL) {
    $typed_entities = [];
    foreach ($entities as $entity) {
      $label = $entity->label();

      // Extract entities belonging to the bundle in question.
      if (!isset($bundle) || $entity->bundle() == $bundle) {
        // Make sure we have a completed loaded entity.
        if ($entity && $label) {
          // Commas and quotes in tag names are special cases, so encode 'em.
          if (strpos($label, ',') !== FALSE || strpos($label, '"') !== FALSE) {
            $label = '"' . str_replace('"', '""', $label) . '"';
          }

          $typed_entities[] = $label;
        }
      }
    }

    return implode(',', $typed_entities);
  }

}
