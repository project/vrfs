<?php

namespace Drupal\vrfs\Plugin\ViewsReferenceSetting;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Utility\Token;
use Drupal\views\ViewExecutable;
use Drupal\viewsreference\Plugin\ViewsReferenceSettingInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The views reference setting pager plugin.
 *
 * @ViewsReferenceSetting(
 *   id = "argument_tokenizer",
 *   label = @Translation("Argument Tokenizer"),
 *   default_value = "",
 * )
 *
 * @SuppressWarnings("UnusedLocalVariable")
 */
class ViewsReferenceArgumentTokenizer extends PluginBase implements ViewsReferenceSettingInterface, ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * Key value service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Utility\Token $token
   *   The token service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Token $token, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->token = $token;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('token'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function alterFormField(array &$form_field) {
    $form_field['#type'] = 'details';
    $form_field['#open'] = TRUE;
    $form_field['#tree'] = TRUE;
    $form_field['#title'] = $this->t('Arguments as tokens');

    if ($this->configuration['display_id'] != NULL) {
      $view_id = $this->configuration['view_name'];
      $display_id = $this->configuration['display_id'];

      /** @var \Drupal\views\ViewExecutable $view */
      $view = $this->entityTypeManager->getStorage('view')->load($view_id)->getExecutable();
      $view->setDisplay($display_id);

      $display_handler = $view->display_handler;
      $arguments = $display_handler->getOption("arguments");

      if (!empty($arguments) && !empty($this->configuration['target_entity_type_id'])) {
        foreach ($arguments as $name => $filter) {
          $form_field[$name] = [
            '#type' => 'textfield',
            '#title' => $name,
            '#default_value' => $form_field["#default_value"][$name] ?? NULL,
          ];
        }
        // Show the token help relevant to this pattern type.
        $form_field['token_help'] = [
          '#theme' => 'token_tree_link',
          '#token_types' => [$this->getTokenType($this->configuration['target_entity_type_id'])],
        ];

      }
    }
  }

  /**
   * Get the token type for the entity type.
   *
   * @param string $entity_type
   *   The entity type.
   *
   * @return string|null
   *   The token type.
   */
  protected function getTokenType(string $entity_type) {
    $entities = \Drupal::entityTypeManager()->getDefinitions();
    if (!empty($entities[$entity_type])) {
      return $entities[$entity_type]->get('token_type');
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function alterView(ViewExecutable $view, $value) {
    if (empty($value)) {
      return;
    }

    $entity = $view->storage->getThirdPartySetting('vrfs', 'entity');
    if (!$entity instanceof ContentEntityBase) {
      return;
    }
    $arguments = $view->display_handler->getOption("arguments");
    $args = [];
    foreach ($arguments as $name => $filter) {
      $args[] = $this->token->replace($value[$name], [$this->getTokenType($entity->getEntityTypeId()) => $entity], ['clear' => TRUE]);
    }
    $view->setArguments($args);
  }

}
