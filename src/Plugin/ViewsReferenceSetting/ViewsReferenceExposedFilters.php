<?php

namespace Drupal\vrfs\Plugin\ViewsReferenceSetting;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\views\ViewExecutable;
use Drupal\viewsreference\Plugin\ViewsReferenceSettingInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The views reference setting pager plugin.
 *
 * @ViewsReferenceSetting(
 *   id = "exposed_filters",
 *   label = @Translation("Exposed Filters"),
 *   default_value = "",
 * )
 */
class ViewsReferenceExposedFilters extends PluginBase implements ViewsReferenceSettingInterface, ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * Key value service.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueFactoryInterface
   */
  protected $keyValue;

  /**
   * {@inheritdoc}
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $key_value
   *   Key value storage.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, KeyValueFactoryInterface $key_value) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->keyValue = $key_value;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('keyvalue')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function alterFormField(array &$form_field) {
    $form_field['#type'] = 'details';
    $form_field['#open'] = TRUE;
    $form_field['#tree'] = TRUE;

    if ($this->configuration['display_id'] != NULL) {
      $view_id = $this->configuration['view_name'];
      $display_id = $this->configuration['display_id'];

      $entity_type_manager = \Drupal::service('entity_type.manager');

      /** @var \Drupal\views\ViewExecutable $view */
      $view = $entity_type_manager->getStorage('view')->load($view_id)->getExecutable();
      $view->setDisplay($display_id);

      $display_handler = $view->display_handler;
      $exposed_filters = $display_handler->getOption("filters");

      if (!empty($exposed_filters)) {
        foreach ($exposed_filters as $name => $filter) {
          if (isset($filter["exposed"])) {
            if (!$filter["exposed"]) {
              continue;
            }

            $form_field[$name] = [
              '#type' => 'checkbox',
              '#title' => $name,
              '#attributes' => [
                'id' => 'field_' . $name,
              ],
              '#default_value' => $form_field["#default_value"][$name] ?? TRUE,
            ];
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function alterView(ViewExecutable $view, $value) {
    if (empty($value)) {
      return;
    }

    foreach ($value as $filter => $filter_value) {
      if ($filter_value == 0) {
        $disabled_filters[] = $filter;
      }
    }

    if (!isset($disabled_filters)) {
      return;
    }

    $handler_filters = $view->display_handler->getOption("filters");

    foreach ($disabled_filters as $disabled_filter) {
      if (isset($handler_filters[$disabled_filter])) {
        $handler_filters[$disabled_filter]["exposed"] = FALSE;
      }
    }

    $view->display_handler->setOption("filters", $handler_filters);
  }

}
